from drv8830 import DRV8830

try:
    print("opening DRV8830")
    pwm = DRV8830()
    pwm.set_throttle(0)
    pwm.coast()
    pwm.reverse()
    pwm.forward()
    pwm.set_throttle(0.5)
    pwm.set_throttle(1)
    pwm.set_throttle(0)
    pwm.brake()
    pwm.reverse()
    pwm.set_throttle(0.5)
    pwm.set_throttle(1)
    pwm.set_throttle(0)
    pwm.coast()

except Exception as e:
    print(f"device error: {e}")
