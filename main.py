from time import sleep
from paj7620 import PAJ7620, Gestures

try:
    print("opening PAJ7620")
    sensor = PAJ7620()

    while True:
        sleep(0.1)
        gesture = sensor.get_gesture()
        if gesture == Gestures.GEST_UP:
            print("Up")
        elif gesture == Gestures.GEST_DOWN:
            print("Down")
        elif gesture == Gestures.GEST_LEFT:
            print("Left")
        elif gesture == Gestures.GEST_RIGHT:
            print("Right")
        elif gesture == Gestures.GEST_FORWARD:
            print("Forward")
        elif gesture == Gestures.GEST_BACKWARD:
            print("Backward")
        elif gesture == Gestures.GEST_CLOCKWISE:
            print("Clockwise")
        elif gesture == Gestures.GEST_COUNTER_CLOCKWISE:
            print("Counter clockwise")

except Exception as e:
    print(f"device error: {e}")
