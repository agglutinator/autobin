from ch341 import CH341

DRV8830_I2C_ADDRESS = 0x64

CMD_REGISTER = 0x00
CMD_COAST = 0b00
CMD_REVERSE = 0b01
CMD_FORWARD = 0b10
CMD_BREAK = 0b11

# VOLTAGE_BITMASK = 0b11111100

class DRV8830:
    def __init__(self, address=DRV8830_I2C_ADDRESS):
        self._address = address
        self._device = CH341()
        self._device.set_speed(100)
        self._current_cmd = None
        self._current_voltage_ratio = None

    def forward(self):
        self._current_cmd = CMD_FORWARD
        if self._current_voltage_ratio:
            self.set_throttle(self._current_voltage_ratio)
        else:
            self._write_u8(CMD_REGISTER, self._current_cmd)

    def reverse(self):
        self._current_cmd = CMD_REVERSE
        if self._current_voltage_ratio:
            self.set_throttle(self._current_voltage_ratio)
        else:
            self._write_u8(CMD_REGISTER, self._current_cmd)

    def coast(self):
        self._current_cmd = CMD_COAST
        if self._current_voltage_ratio:
            self.set_throttle(self._current_voltage_ratio)
        else:
            self._write_u8(CMD_REGISTER, self._current_cmd)

    def brake(self):
        self._current_cmd = CMD_BREAK
        if self._current_voltage_ratio:
            self.set_throttle(self._current_voltage_ratio)
        else:
            self._write_u8(CMD_REGISTER, self._current_cmd)

    def set_throttle(self, ratio):
        # 4 x VREF x (VSET +1) / 64
        # VREF = 1.285V
        # VSET= 0x06h ..  0x3Fh
        # ratio is 0..1 of a VCC
        reg_value = int(57 * ratio + 6)
        reg_value = reg_value << 2
        if self._current_cmd:
            reg_value += self._current_cmd
        self._write_u8(CMD_REGISTER, reg_value)

    def _write_u8(self, register, data):
        self._device.write_byte_data(self._address, register, data)
        print(f"addr: 0x{self._address:02x}, reg:0x{register:02x}, hex=0x{data:02x}, bin={data:08b}")
